#!/bin/sh

MDBOOK_VERSION=$(cargo search mdbook | grep "^mdbook =" | sed -e "s/mdbook = \"\(.*\)\"[[:space:]]\+#.*/\1/")
echo $MDBOOK_VERSION
sed -e "s/{{MDBOOK_VERSION}}/${MDBOOK_VERSION}/" template.Dockerfile > Dockerfile
echo export "MDBOOK_VERSION=$MDBOOK_VERSION" > mdbook_version.env
